package cat.xtec.ioc.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;

import cat.xtec.ioc.SpaceRace;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.helpers.InputHandler;
import cat.xtec.ioc.objects.Asteroid;
import cat.xtec.ioc.objects.Bala;
import cat.xtec.ioc.objects.ScrollHandler;
import cat.xtec.ioc.objects.Spacecraft;
import cat.xtec.ioc.utils.Settings;


public class GameScreen implements Screen {

    // Els estats del joc

    public enum GameState {

        READY, RUNNING, GAMEOVER

    }

    private GameState currentState;

    // Objectes necessaris
    private Stage stage;
    private Spacecraft spacecraft;
    //private Bala bala;
    private ScrollHandler scrollHandler;

    // Encarregats de dibuixar elements per pantalla

    private Batch batch;

    // Per controlar l'animació de l'explosió
    private float explosionTime = 0;

    // Preparem el textLayout per escriure text
    private GlyphLayout textLayout;

    private GlyphLayout puntuacioText;
    private int puntuacio;
    public boolean pausado=false;
    public String dificultat ="";
    private int suma=10;
    private int contador = 0;
    private int vides = 5;
    private int generar_balas;
    private int num_balas;
    private int num_balas_or = 10;
    private int generar_balas_or = 100;



    public GameScreen(Batch prevBatch, Viewport prevViewport, String game) {


        // Iniciem la música
        AssetManager.music.play();
        this.dificultat = game;
        // Creem l'stage i assginem el viewport
        stage = new Stage(prevViewport, prevBatch);

        batch = stage.getBatch();

        // Creem la nau i la resta d'objectes
        spacecraft = new Spacecraft(Settings.SPACECRAFT_STARTX, Settings.SPACECRAFT_STARTY, Settings.SPACECRAFT_WIDTH, Settings.SPACECRAFT_HEIGHT,stage);
        scrollHandler = new ScrollHandler();
        //bala = new Bala(actor.getX()+actor.getWidth(), (actor.getY()+actor.getHeight()/2), 52, 20, scrollHandler)
        // Afegim els actors a l'stage
        stage.addActor(scrollHandler);
        stage.addActor(spacecraft);
        // Donem nom a l'Actor
        spacecraft.setName("spacecraft");

        // Iniciem el GlyphLayout
        textLayout = new GlyphLayout();
        textLayout.setText(AssetManager.font, "Seguro que estas\nlisto?");
        puntuacioText = new GlyphLayout();
        puntuacio = 0;
        currentState = GameState.READY;

        // Assignem com a gestor d'entrada la classe InputHandler
        Gdx.input.setInputProcessor(new InputHandler(this));

        if(game.equals("FACIL")) {
            scrollHandler.facil();
            suma=5;
            vides = 5;
            generar_balas_or = 50;
            num_balas_or = 50;

        } else if (game.equals("NORMAL")){
            scrollHandler.normal();
            suma=10;
            vides = 3;
            generar_balas_or = 100;
            num_balas_or = 30;
        } else if (game.equals("DIFICIL")){
            scrollHandler.dificil();
            suma=15;
            vides = 2;
            generar_balas_or = 150;
            num_balas_or = 15;
        }
        num_balas = num_balas_or;
        generar_balas = generar_balas_or;

    }

    public void setNum_balas(int num_balas) {
        this.num_balas = num_balas;
    }

    public int getNum_balas() {
        return num_balas;
    }




    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        // Dibuixem tots els actors de l'stage
        stage.draw();

        // Depenent de l'estat del joc farem unes accions o unes altres
        switch (currentState) {

            case GAMEOVER:
                updateGameOver(delta);
                break;
            case RUNNING:
                updateRunning(delta);
                break;
            case READY:
                updateReady();
                break;


        }

        //drawElements();

    }

    private void updateReady() {

        // Dibuixem el text al centre de la pantalla
        batch.begin();
        AssetManager.font.draw(batch, textLayout, (Settings.GAME_WIDTH / 2) - textLayout.width / 2, (Settings.GAME_HEIGHT / 2) - textLayout.height / 2);

        batch.end();

    }

    private void updateRunning(float delta) {
        batch.begin();
        puntuacioText = AssetManager.fontPuntuacio.draw(batch, puntuacio+ "puntos", Settings.GAME_WIDTH - puntuacioText.width/3-80, 0);
        stage.act(delta);
        if (contador == 60){
            puntuacio=puntuacio+suma;
            contador = 0;
        }else{
            contador++;
        }

        if (scrollHandler.collides(spacecraft)) {
            String showPoints = Integer.toString(puntuacio);
            puntuacio = 0;
            AssetManager.explosionSound.play();
            stage.getRoot().findActor("spacecraft").remove();
            textLayout.setText(AssetManager.font, "Game Over\n" +
                    showPoints+" puntos");
            currentState = GameState.GAMEOVER;
        }

        if(puntuacio >= generar_balas){
            num_balas += 1;
            generar_balas += generar_balas_or;
        }
        AssetManager.fontPuntuacio.draw(batch, "Balas: " + num_balas, 0, 0);
        if (vides == 1){
            AssetManager.fontPuntuacio.draw(batch, vides + " vida", Settings.GAME_WIDTH - puntuacioText.width, Settings.GAME_HEIGHT - puntuacioText.height );
        }else{
            AssetManager.fontPuntuacio.draw(batch, vides + " vidas", Settings.GAME_WIDTH - puntuacioText.width, Settings.GAME_HEIGHT - puntuacioText.height );
        }
        batch.end();
    }

    private void updateGameOver(float delta) {
        stage.act(delta);


        batch.begin();
        AssetManager.font.draw(batch, textLayout, (Settings.GAME_WIDTH - textLayout.width) / 2, (Settings.GAME_HEIGHT - textLayout.height) / 2);
        // Si hi ha hagut col·lisió: Reproduïm l'explosió i posem l'estat a GameOver
        batch.draw((TextureRegion) AssetManager.explosionAnim.getKeyFrame(explosionTime, false), (spacecraft.getX() + spacecraft.getWidth() / 2) - 32, spacecraft.getY() + spacecraft.getHeight() / 2 - 32, 64, 64);

        batch.end();

        explosionTime += delta;

    }

    public void reset() {

        if(vides > 1) {
            vides = vides-1;
            if (vides == 1){
                textLayout.setText(AssetManager.font, "Estas listo?\n\n"
                        + "Esta es tu ultima vida!!");
            }else {
                textLayout.setText(AssetManager.font, "Estas listo?\n\n"
                        + vides + " vidas restantes");
            }
            spacecraft.reset();
            scrollHandler.reset();
            currentState = GameState.READY;
            stage.addActor(spacecraft);
            explosionTime = 0.0f;
            num_balas = num_balas_or;

        } else{
            textLayout.setText(AssetManager.font, "has perdido...\nNo te quedan vidas");

        }

    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public Spacecraft getSpacecraft() {
        return spacecraft;
    }

    public Stage getStage() {
        return stage;
    }

    public ScrollHandler getScrollHandler() {
        return scrollHandler;
    }

    public GameState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(GameState currentState) {
        this.currentState = currentState;
    }
}
