package cat.xtec.ioc.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import cat.xtec.ioc.SpaceRace;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.utils.Settings;


public class BaseScreen implements Screen,EventListener {

    protected SpaceRace game;
    private Stage stage;
    private TextButton facil, normal, dificil;

    public BaseScreen(SpaceRace game) {
        TextButton.TextButtonStyle estilo = new TextButton.TextButtonStyle();
        estilo.font = AssetManager.font;
        this.game = game;
        OrthographicCamera camera = new OrthographicCamera(Settings.GAME_WIDTH, Settings.GAME_HEIGHT);
        camera.setToOrtho(true);
        StretchViewport viewport = new StretchViewport(Settings.GAME_WIDTH, Settings.GAME_HEIGHT, camera);
        stage = new Stage(viewport);
        stage.addActor(new Image(AssetManager.background));
        Image spacecraft = new Image(AssetManager.spacecraft);
        float y = Settings.GAME_HEIGHT / 3;
        spacecraft.addAction(Actions.repeat(RepeatAction.FOREVER, Actions.sequence(Actions.moveTo(0 - spacecraft.getWidth(), y), Actions.moveTo(Settings.GAME_WIDTH, y, 5))));
        stage.addActor(spacecraft);
        facil = new TextButton("FACIL", estilo);
        normal = new TextButton("NORMAL", estilo);
        dificil = new TextButton("DIFICIL", estilo);
        Container contenidorFacil = new Container(facil);
        contenidorFacil.setTransform(true);
        contenidorFacil.center();
        contenidorFacil.setPosition(Settings.GAME_WIDTH/2, 40);
        Container contenidorNormal = new Container(normal);
        contenidorNormal.setTransform(true);
        contenidorNormal.center();
        contenidorNormal.setPosition(Settings.GAME_WIDTH/2, 70);
        Container contenidorDificil = new Container(dificil);
        contenidorDificil.setTransform(true);
        contenidorDificil.center();
        contenidorDificil.setPosition(Settings.GAME_WIDTH/2, 100);
        stage.addActor(contenidorFacil);
        stage.addActor(contenidorNormal);
        stage.addActor(contenidorDificil);
        facil.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                BaseScreen.this.game.setScreen(new GameScreen(BaseScreen.this.stage.getBatch(), BaseScreen.this.stage.getViewport(), "FACIL"));
                dispose();
            }
        });
        normal.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                BaseScreen.this.game.setScreen(new GameScreen(BaseScreen.this.stage.getBatch(), BaseScreen.this.stage.getViewport(), "NORMAL"));
                dispose();
            }
        });
        dificil.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                BaseScreen.this.game.setScreen(new GameScreen(BaseScreen.this.stage.getBatch(), BaseScreen.this.stage.getViewport(), "DIFICIL"));
                dispose();
            }
        });
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        stage.draw();
        stage.act(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean handle(Event event) {
        return false;
    }

}
